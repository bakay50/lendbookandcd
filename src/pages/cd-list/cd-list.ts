import {Component} from '@angular/core';
import {MenuController, ModalController} from 'ionic-angular';
import {LendCdPage} from "../lend-cd/lend-cd";
import {Cd} from "../../models/Cd";
import {CdService} from "../../services/cd.service";


@Component({
  selector: 'page-cd-list',
  templateUrl: 'cd-list.html',
})
export class CdListPage {

  cdsList: Cd[];

  constructor(private menuCtrl: MenuController, public cdService: CdService, private modalCtrl: ModalController) {
  }

  ionViewWillEnter() {
    this.cdsList = this.cdService.cdsList.slice();
  }

  onLoadCd(index: number) {
    let modal = this.modalCtrl.create(LendCdPage, {index: index});
    modal.present();
  }

  onToggleMenu() {
    this.menuCtrl.open();
  }

}
