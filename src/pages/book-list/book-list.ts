import {Component} from '@angular/core';
import {MenuController, ModalController} from 'ionic-angular';
import {Book} from "../../models/Book";
import {BooksService} from "../../services/books.service";
import {LendBookPage} from "../lend-book/lend-book";

@Component({
  selector: 'page-book-list',
  templateUrl: 'book-list.html',
})
export class BookListPage {

  booksList: Book[];

  constructor(private menuCtrl: MenuController, public booksService: BooksService, private modalCtrl: ModalController) {
  }

  ionViewWillEnter() {
    this.booksList = this.booksService.booksList.slice();
  }

  onLoadBook(index: number) {
    let modal = this.modalCtrl.create(LendBookPage, {index: index});
    modal.present();
  }

  onToggleMenu() {
    this.menuCtrl.open();
  }

}
