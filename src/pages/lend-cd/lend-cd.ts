import {Component} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';
import {Cd} from "../../models/Cd";
import {CdService} from "../../services/cd.service";


@Component({
  selector: 'page-lend-cd',
  templateUrl: 'lend-cd.html',
})
export class LendCdPage {

  index: number;
  cd: Cd;

  constructor(public navParams: NavParams,
              public viewCtrl: ViewController,
              public cdService: CdService) {
  }

  ngOnInit() {
    this.index = this.navParams.get('index');
    this.cd = this.cdService.cdsList[this.index];
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }

  onToggleCd() {
    this.cdService.onToggleCd(this.cd);
  }

}
