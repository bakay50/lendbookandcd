import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {MyApp} from './app.component';
import {TabsPage} from "../pages/tabs/tabs";
import {SettingsPage} from "../pages/settings/settings";
import {LendCdPage} from "../pages/lend-cd/lend-cd";
import {LendBookPage} from "../pages/lend-book/lend-book";
import {CdListPage} from "../pages/cd-list/cd-list";
import {BookListPage} from "../pages/book-list/book-list";
import {BooksService} from "../services/books.service";
import {CdService} from "../services/cd.service";

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    SettingsPage,
    LendCdPage,
    LendBookPage,
    CdListPage,
    BookListPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    SettingsPage,
    LendCdPage,
    LendBookPage,
    CdListPage,
    BookListPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CdService,
    BooksService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}
