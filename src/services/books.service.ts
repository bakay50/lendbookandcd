import {Book} from "../models/Book";


export class BooksService {
  booksList: Book[] = [
    {
      titre: 'Le monde s\'effondre',
      auteur:'Chinua Achebe',
      genre:'roman',
      description:['description : À travers le destin d\'Okonkwo, un notable de son clan, Chinua Achebe évoque le choc culturel qu\'a représenté \n' +
      'pour les autochtones l\'arrivée des Britanniques à Igbos, à la fin du xixe siècle \n' +
      'et la colonisation du Nigeria par les Britanniques'],
      prix:2500,
      isLend: false
    },
    {
      titre: 'Contes',
      auteur:'Hans Christian Andersen',
      genre:'recueil de contes',
      description: [
        'description :À partir de 1843, l\'écrivain s\'est défendu d\'avoir écrit ses contes seulement pour les enfants. Pourtant les recueils publiés de 1832 à 1842 ' +
        'en six brochures, portent bien le titre : ' +
        'Contes pour enfants, titre qu\'il ne reprendra pas, une fois la gloire venue, dans sa deuxième série de 1843-1848'
      ],
      prix:2700,
      isLend: true
    },
    {
      titre: 'Le Père Goriot',
      auteur:'Honoré de Balzac',
      genre:'roman',
      description: [
        'description :Le Père Goriot aborde le thème de l\'amour paternel poussé jusqu\'à la déraison. ' +
        'Il donne aussi une vision globale de la société parisienne sous la Restauration ' +
        'et de toutes ses couches sociales, depuis les plus démunies jusqu\'aux plus élevées'
      ],
      prix:3500,
      isLend: true
    },
    {
      titre: 'Divine Comédie',
      auteur:'Dante Alighieri',
      genre:'Poésie',
      description: [
        'description :Elle est également considérée comme le premier grand texte en italien : la langue dans laquelle elle est écrite a eu une influence considérable sur ' +
        'l\'idiome moderne de la péninsule. Pour écrire son œuvre, Dante a été très largement inspiré par le sanglant conflit'+
        'qu\'il a lui-même vécu en Italie, opposant les Guelfes (Guelfi) et les Gibelins (Ghibellini) (1125-1300)'
      ],
      prix:3500,
      isLend: false
    }
  ];


  onToggleBook(book:Book) {
    book.isLend = !book.isLend;
  }

}
