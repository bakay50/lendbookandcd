import {Cd} from "../models/Cd";


export class CdService {
  cdsList: Cd[] = [
    {
      titre: 'Love Me Do',
      auteur:'The Beatles',
      description: [
        'description:The Beatles est un groupe musical britannique, originaire de Liverpool, en Angleterre. Formé en 1960, ' +
        'et composé de John Lennon, Paul McCartney, George Harrison et Ringo Starr, il est considéré comme le groupe le plus populaire et influent de l\'histoire du rock.'+
        'En dix ans d\'existence et seulement huit ans de carrière discographique (de 1962 à 1970)'
      ],
      prix:2800,
      isLend: true
    },
    {
      titre: 'Loving You',
      auteur:'Elvis Presley',
      description: [
        'description :Elvis Aaron Presley est né le 8 janvier 1935 à Tupelo, dans l\'État du Mississippi'+
        'dans une petite maison de type shotgun house construite par son père,'+
        'Vernon Elvis Presley (1916-1979), en prévision de sa naissance'
      ],
      prix:2300,
      isLend: true
    },
    {
      titre: 'Ropin\' the Wind',
      auteur:'Garth Brooks',
      description: [
        'description : Garth Brooks connait un succès commercial phénoménal depuis les années 1990 avec plus de 70 chansons à succès lui permettant de vendre plus de 130 millions ' +
        'd\'albums aux États-Unis, ce qui fait de lui l\'un des meilleurs vendeurs de disques de tous les temps aux États-Unis'
      ],
      prix:3500,
      isLend: false
    },
    {
      titre: 'Custard Pie',
      auteur:'Led Zeppelin',
      description: [
        'description : Led Zeppelin est un groupe de rock britannique, originaire de Londres, en Angleterre. Il est fondé en 1968 par Jimmy Page (guitare), ' +
        'avec Robert Plant (chant), John Paul Jones (basse, claviers) et John Bonham (batterie), ' +
        'et dissous à la suite de la mort de ce dernier'
      ],
      prix:3700,
      isLend: false
    }
  ];

  onToggleCd(cd :Cd){
    cd.isLend = !cd.isLend;
  }
}
