export class Cd {
  titre: string;
  auteur: string;
  description: string[];
  prix: number;
  isLend: boolean;

  constructor(titre: string, auteur: string, description: string[], prix: number, isLend: boolean) {
    this.titre = titre;
    this.auteur = auteur;
    this.description = description;
    this.prix = prix;
    this.isLend = isLend;
  }
}
