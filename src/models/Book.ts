export class Book {
  titre: string;
  auteur: string;
  genre: string;
  description: string[];
  prix: number;
  isLend: boolean;

  constructor(titre: string, auteur: string, genre: string, description: string[], prix: number, isLend: boolean) {
    this.titre = titre;
    this.auteur = auteur;
    this.genre = genre;
    this.description = description;
    this.prix = prix;
    this.isLend = isLend;
  }
}
